$(document).ready(function() {
	var inputFrom = $(".js-input-from");
	var inputTo = $(".js-input-to");
	$( ".js-slider-ui" ).slider({
		range: true,
		min: 0,
		max: 50000,
		values: [ 800, 45000 ],
		slide: function( event, ui ) {
		  inputFrom.val(ui.values[ 0 ]);
		  inputTo.val(ui.values[ 1 ]);
		}
    });
    inputFrom.val($( ".js-slider-ui" ).slider( "values", 0 ));
    inputTo.val($( ".js-slider-ui" ).slider( "values", 1 ));
		
});
