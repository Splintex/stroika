$(document).ready(function() {
	document.createElement( "picture" );

	var bigProduct = $(".js-big-product");
	var smProduct = $(".js-small-product");
	var verProduct = $(".js-vertical-product");
	var verProductTwo = $(".js-vertical-product-two");
	var nav = $(".js-nav");
	var body = $("body");
	
	if (verProduct.children().length > 13) {
		verProduct.addClass("is-disabled-dots");
	}

	$(".js-partners").slick({
		slidesToShow: 4,
		slidesToScroll: 4,
		infinite: true,
		arrows: true,
		responsive:[
			{
				breakpoint: 1000,
				settings:{
					slidesToShow: 3,
					slidesToScroll: 3,
					arrows: true,
				}
			}
		]
	});
	bigProduct.on('init', function(slick) {
		  setTimeout(function(){
		  	bigProduct.addClass("is-ready");
		  },200);
	});
	bigProduct.slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		responsive:[
			{
				breakpoint: 767,
				settings:{
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: true,
					dots: true
				}
			}
		]
	});
	$(".js-btn-prev").on("click", function(){
	  $(this).parents(".js-nav").find(".slick-prev").trigger("click");
	  return false;
	});
	$(".js-btn-next").on("click", function(){
	  $(this).parents(".js-nav").find(".slick-next").trigger("click");
	  return false;
	});

	$(".js-slider").on('init', function(slick) {
		  setTimeout(function(){
		  	nav.addClass("is-ready");
		  },200);
		// $(".js-slider").find(".slick-dots li").clone().appendTo(".js-slider-dots");
	});
	$(".js-slider-two").on('init', function(slick) {
		  setTimeout(function(){
		  	nav.addClass("is-ready");
		  },200);
		// $(".js-slider-two").find(".slick-dots li").clone().appendTo(".js-slider-dots-two");
	});
	$(".js-slider").slick({
	  infinite: true,
	  slidesToShow: 4,
	  slidesToScroll: 4,
	  dots: true,
	  arrows: true,
	  responsive:[
		{
			breakpoint: 1000,
		  	settings:{
		  		slidesToShow: 3,
		  		slidesToScroll: 3,
		  		arrows: true,
		  		dots: true
		  	}
		},
	  	{
	  		breakpoint: 767,
	  		settings:{
	  			slidesToShow: 2,
	  			slidesToScroll: 2,
	  			arrows: true,
	  			dots: true
	  		}
	  	},
	  	{
	  		breakpoint: 374,
	  		settings:{
	  			slidesToShow: 1,
	  			slidesToScroll: 1,
	  			arrows: true,
	  			dots: true
	  		}
	  	}
	  ]
	});
	$(".js-slider-two").slick({
	  infinite: true,
	  slidesToShow: 4,
	  slidesToScroll: 4,
	  dots: true,
	  arrows: true,
	  responsive:[
		{
			breakpoint: 1000,
		  	settings:{
		  		slidesToShow: 3,
		  		slidesToScroll: 3,
		  		arrows: true,
		  		dots: true
		  	}
		},
	  	{
	  		breakpoint: 767,
	  		settings:{
	  			slidesToShow: 2,
	  			slidesToScroll: 2,
	  			arrows: true,
	  			dots: true
	  		}
	  	},
	  	{
	  		breakpoint: 374,
	  		settings:{
	  			slidesToShow: 1,
	  			slidesToScroll: 1,
	  			arrows: true,
	  			dots: true
	  		}
	  	}
	  ]
	});
	// var sliderDot = $(".js-slider-dots li");
	// sliderDot.on("click", function(){
	//   var index = $(this).index();
	//   sliderDot.removeClass("slick-active");
	//   $(this).addClass("slick-active");
	//   $(this).parents(".slick-dots li").eq(index).trigger("click");
	//   //$(".js-slider").slick('slickGoTo', index);
	//   return false;
	// });
	// var sliderDotTwo = $(".js-slider-dots-two li");
	// sliderDotTwo.on("click", function(){
	//   var index = $(this).index();
	//   sliderDotTwo.removeClass("slick-active");
	//   $(this).addClass("slick-active");
	//   $(".js-slider-two").slick('slickGoTo', index);
	//   return false;
	// });
	// $(".js-slider").on('beforeChange', function(event, slick, currentSlide, nextSlide){
	//   sliderDot.removeClass("slick-active");
	//   sliderDot.eq(nextSlide).addClass("slick-active")
	// });
	// $(".js-slider-two").on('beforeChange', function(event, slick, currentSlide, nextSlide){
	//   sliderDotTwo.removeClass("slick-active");
	//   sliderDotTwo.eq(nextSlide).addClass("slick-active")
	// });
	
	smProduct.on('init', function(slick) {
		  setTimeout(function(){
		  	smProduct.addClass("is-ready");
		  },200);
	});
	smProduct.slick({
		slidesPerRow: 4,
		rows: 2,
		dots: true,
		responsive:[
			{
				breakpoint: 1000,
				settings:{
					slidesPerRow: 2,
					rows: 2,
					dots: true,
				}
			},
			{
				breakpoint: 767,
				settings:{
					slidesPerRow: 2,
					rows: 2,
					dots: true,
				}
			}
		]
	});
	verProduct.slick({
		vertical: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		arrows: true,
		dots: false,
		dragable: true,
		verticalSwiping: true,
		responsive:[
			{
				breakpoint: 767,
				settings:{
					vertical: false,
					verticalSwiping: false,
					slidesToShow: 2,
					slidesToScroll: 2,
					arrows: true,
					dots: true
				}
			},
			{
				breakpoint: 350,
				settings:{
					vertical: false,
					verticalSwiping: false,
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: true,
					dots: true
				}
			}
		]
	});

	verProductTwo.slick({
		vertical: true,
		verticalSwiping: true,
		slidesToShow: 2,
		slidesToScroll: 2,
		arrows: true,
		dots: false,
		responsive:[
			{
				breakpoint: 767,
				settings:{
					vertical: false,
					verticalSwiping: false,
					slidesToShow: 2,
					slidesToScroll: 2,
					arrows: true,
					dots: true
				}
			},
			{
				breakpoint: 350,
				settings:{
					vertical: false,
					verticalSwiping: false,
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: true,
					dots: true
				}
			}
		]
	});
	verProductTwo.on('init', function(slick) {
		  setTimeout(function(){
		  	smProduct.addClass("is-ready");
		  },200);
		  return false;
	});
	$(".js-btn-menu").on("click", function(){
		$(".js-menu").addClass("is-active");
		return false;
	});
	$(".js-menu-close").on("click",function(){
		$(".js-menu").removeClass("is-active")
	});
	$(".js-select").on("click",function(){
		$(this).toggleClass("is-active");
		$(".js-select-list").toggleClass("is-active");
	});
	$(".js-select-list li ").on("click", function() {
		var text = $(this).attr("data-value");
		var index = $(this).index();
		var select = $(this).parents(".js-select").find("select");
		$(this).parents(".search").find(".js-input").text(text);
		$(this).parents(".js-select-list").toggleClass("is-active");
		select.find("option").attr("selected", false);
		select.find("option").eq(index).attr("selected", true);
		return false;
	});
	// $(".js-select-filter-btn").on("click",function(){
	// 	$(this).parents(".js-select-filter").toggleClass("is-active");
	// });
	$(".js-select-filter").on("click",function(){
		if ($(this).hasClass("is-active")) {
			$(".js-select-filter").removeClass("is-active");
		}
		else {
			$(".js-select-filter").removeClass("is-active");
			$(this).addClass("is-active");
		}
		
	});
	$(".js-section-item").on("click",function(){
		if ($(this).hasClass("is-active")) {
			$(".js-section-item").removeClass("is-active");
		}
		else {
			$(".js-section-item").removeClass("is-active");
			$(this).addClass("is-active");
		}
	});
	$(".js-section-item-mob").on("click", function(){
		if ($(this).hasClass("is-active")) {
			$(".js-section-item-mob").removeClass("is-active");
			$(this).find(".js-section-list").slideUp(200);
		}
		else {
			$(".js-section-list").hide();
			$(".js-section-item-mob").removeClass("is-active");
			$(this).find(".js-section-list").slideDown(200);
			$(this).addClass("is-active");
		}
		return false;
	});
	$(".js-select-filter-list li ").on("click", function() {
		  var text = $(this).attr("data-value");
		  var filter = $(this).parents(".js-select-filter");
		  var select = filter.find("select");
		  var arrow = $(this).find(".js-data-arrow").attr("data-arrow");
		  var index = $(this).index();
		  filter.find(".js-input").text(text);
		  filter.toggleClass("is-active");
		  select.find("option").attr("selected", false);
		  select.find("option").eq(index).attr("selected", true);
		  filter.find(".js-arrow").text(arrow);
		  return false;
	});


	$(".js-btn-close").on("click",function(){
		body.removeAttr("class");
		return false;
	});
	$(".js-btn-open").on("click",function(){
		var bodyClass = $(this).attr("data-body-class");
		body.toggleClass(bodyClass);
		return false;
	});

	// $(".js-btn-section").on("click",function(){
	// 	body.toggleClass("has-open-section");
	// 	return false;
	// });
	// $(".js-btn-filter").on("click",function(){
	// 	body.toggleClass("has-open-filter");
	// 	return false;
	// });
	$(".js-to-show").on("click",function(){
		$(this).addClass("is-active");
		$(this).parents(".item").find(".js-to-hide").addClass("is-active");
		return false;
	});
	$(".js-raty").raty({
		score: function() {
			return $(this).attr('data-score');
		},
		hints: ['1', '2', '3', '4', '5'],
		path: 'img/svg',
		starOn: 'star-act.svg',
		starOff: 'star.svg',
	});
	$('.js-raty-readonly').raty({
		readOnly: true,
		score: function() {
		    return $(this).attr('data-score');
		},
		hints: ['1', '2', '3', '4', '5'],
		path:      'img/svg',
		starOn:    'star-act.svg',
		starOff:   'star.svg',
		half: true,
		starHalf: 'star-half.svg'
	});
	$('.js-gallery-sync').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
	  fade: true,
	  asNavFor: '.js-gallery'
	});
	$('.js-gallery').slick({
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  asNavFor: '.js-gallery-sync',
	  dots: false,
	  focusOnSelect: true,
	  responsive:[
	  	{
	  		breakpoint: 400,
	  		settings:{
	  			slidesToShow: 2,
				slidesToScroll: 1,
				asNavFor: '.js-gallery-sync',
				dots: false,
				focusOnSelect: true,
	  		}
	  	},
	  ]
	});  
	$(".js-sorted").on("click",function(){
		$(".js-sorted").removeClass("is-active");
		$(this).addClass("is-active").toggleClass("is-sorted");
		return false;
	});
	$(".js-accord").on("click", function(){
		$(this).toggleClass("is-open");
	});
	$(".js-counter").each(function() {
		var dateArr = $(this).attr("data-date").split(".")
		var date = new Date(dateArr[2], dateArr[1]-1, dateArr[0])
		$(this).countdown({
			until: date,
			format: 'dHMs',
			compact: true
		});
	});
	$(".js-file-btn").on("click", function (){
	    $(this).next().trigger("click");
	    return false;
	});
    $(".js-file-input").on("change", function (){
        $(this).parents(".js-file").find(".js-file-name").val(this.files["0"].name);
    });
    $(".js-enter").on("click",function(){
    	$(this).toggleClass("is-active");
    	$(".js-popup").toggleClass("is-active");
    });
    var inputFrom = $(".js-input-from");
	var inputTo = $(".js-input-to");
	$( ".js-slider-ui" ).slider({
		range: true,
		min: 0,
		max: 50000,
		values: [ 800, 45000 ],
		slide: function( event, ui ) {
		  inputFrom.val(ui.values[ 0 ]);
		  inputTo.val(ui.values[ 1 ]);
		}
    });
    inputFrom.val($( ".js-slider-ui" ).slider( "values", 0 ));
    inputTo.val($( ".js-slider-ui" ).slider( "values", 1 ));

    $('.collapse').collapse({
    	toggle: false
    });
    $(".js-overlay").on("click touchstart", function (){
    	body.removeAttr("class");
        return false;
    });

    $(".js-abc li").on("click", function() {
    	var index = $(this).index();
    	var target = $(".js-abc-list > div").eq(index);
    	
    	$(".js-abc li").removeClass("is-active");
    	$(this).addClass("is-active");
    	
    	$(".js-abc-list > div").removeClass("is-active");
    	target.addClass("is-active");
    });

    $(".js-close-window").on("click", function() {
    	$(this).parents(".js-window").fadeOut(200);
    	return false;
    });
    $(".js-open-window").on("click", function() {
    	var win = $(this).attr("data-window");
    	$("."+win).fadeIn(200);
    	return false;
    });
});
